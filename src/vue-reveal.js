import Vue from 'vue';
import 'reveal.js/css/reveal.css';
import 'reveal.js/css/theme/black.css';
import RevealSlides from '@/components/RevealSlides.vue';

Vue.component('reveal-slides', RevealSlides);
