import Vue from 'vue';
import App from './App.vue';
import './vue-reveal';
import CharTable from '@/components/CharTable.vue';
import CharRow from '@/components/CharRow.vue';

Vue.config.productionTip = false;

Vue.component('char-table', CharTable);
Vue.component('char-row', CharRow);

new Vue({
  render: h => h(App),
}).$mount('#app');
