class CodePoint {
  /**
   * @param {string} code
   */
  constructor(code) {
    this.code = code;
    this.key = Math.random();
  }

  get char() {
    return `&#x${this.code};`;
  }

  get ruby() {
    switch (this.code) {
      case '200D': return 'ZWJ';
      case 'E0062': return '(b)';
      case 'E0065': return '(e)';
      case 'E0067': return '(g)';
      case 'E006E': return '(n)';
      case 'E007F': return '(fin)';
      default: return this.char;
    }
  }

  get unicode() {
    return `U+${this.code}`;
  }

  get utf8hex() {
    const code = parseInt(this.code, 16);

    if (code < 0x10) return `0${code.toString(16).toUpperCase()}`;
    if (code < 0x80) return code.toString(16).toUpperCase();

    if (code < 0x800) {
      const c1 = 0b11000000 + (Math.floor(code / 0b1000000));
      const c2 = 0b10000000 + (code % 0b1000000);
      return (c1 * 0x100 + c2).toString(16).toUpperCase();
    }

    if (code < 0x10000) {
      const c1 = 0b11100000 + (Math.floor(code / 0b1000000000000));
      const c2 = 0b10000000 + (Math.floor((code % 0b1000000000000) / 0b1000000));
      const c3 = 0b10000000 + (code % 0b1000000);
      return (c1 * 0x10000 + c2 * 0x100 + c3).toString(16).toUpperCase();
    }

    if (code < 0x200000) {
      // eslint-disable-next-line
      const c1 = 0b11110000 + ((code & 0b111000000000000000000) >>> 18);
      // eslint-disable-next-line
      const c2 = 0b10000000 + ((code & 0b111111000000000000) >>> 12);
      // eslint-disable-next-line
      const c3 = 0b10000000 + ((code & 0b111111000000) >> 6);
      // eslint-disable-next-line
      const c4 = 0b10000000 + (code & 0b111111);
      return (c1 * 0x1000000 + c2 * 0x10000 + c3 * 0x100 + c4).toString(16).toUpperCase();
    }

    return 'unknown';
  }

  static parse(text) {
    return new CodePoint(text.replace(/^U\+(.*)$/, '$1'));
  }
}

class CodePointCollection {
  /**
   * @param {CodePoint[]} codePoints
   */
  constructor(codePoints) {
    this.codePoints = codePoints;
  }

  get char() {
    return this.codePoints.map(p => p.char).join('');
  }

  get unicode() {
    return this.codePoints.map(p => p.unicode).join(' ');
  }

  get utf8hex() {
    return this.codePoints.map(p => p.utf8hex).join('');
  }

  asEnumerable() {
    return this.codePoints.slice();
  }

  static parse(text) {
    const points = text.split(' ')
      .map(t => CodePoint.parse(t));
    return new CodePointCollection(points);
  }
}

export {
  CodePoint,
  CodePointCollection,
};
